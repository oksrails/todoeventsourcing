﻿using Xunit;
using System;
using static ToDoEventSourcing.Projects.Domain.Events;
using ToDoEventSourcing.Projects.Domain;
using System.Text;

namespace ToDoEventSourcing.Projects.Domain.Tests
{
    public class ProjectTitleTest
    {
        [Fact]
        public void Create_project_title_succes()
        {
            string projectTitleInput = "Project title test";
            ProjectTitle projectTitle = ProjectTitle.FromString(projectTitleInput);

            Assert.NotNull(projectTitle);
            Assert.Equal(projectTitleInput, projectTitle);
        }

        [Fact]
        public void Create_project_title_with_empty_string_exception()
        {
            string projectTitleInput = string.Empty;

            Assert.Throws<ProjectDomainException>(() => ProjectTitle.FromString(projectTitleInput));
        }

        [Fact]
        public void Create_project_title_with_less_then_10_string_exception()
        {
            string projectTitleInput = "fail";

            Assert.Throws<ProjectDomainException>(() => ProjectTitle.FromString(projectTitleInput));
        }

        [Fact]
        public void Create_project_title_with_more_then_100_string_exception()
        {
            string projectTitleInput = TestTools.GenerateRandomString(101);

            Assert.Throws<ProjectDomainException>(() => ProjectTitle.FromString(projectTitleInput));
        }
    }
}
