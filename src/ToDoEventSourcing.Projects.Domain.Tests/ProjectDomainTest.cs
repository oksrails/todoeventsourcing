using System;
using ToDoEventSourcing.Projects.Domain;
using Xunit;

namespace ToDoEventSourcing.Projects.Domain.Tests
{
    public class ProjectDomainTest
    {
        private readonly Project _project;

        public ProjectDomainTest()
        {
            _project = Project.CreateProject(
                ProjectId.FromGuid(Guid.NewGuid()),
                UserId.FromGuid(Guid.NewGuid()));
        }

        [Fact]
        public void Project_title_change_success_test()
        {
            ProjectTitle newProjectTitle = ProjectTitle.FromString("New test project title");

            var result = _project.SetProjectTitle(newProjectTitle);

            Assert.NotNull(result);
            Assert.Equal(newProjectTitle, result.ProjectTitle);
        }

        [Fact]
        public void Project_description_change_success_test()
        {
            ProjectDescription newProjectDescription = ProjectDescription.FromString("New test project description");

            var result = _project.SetProjectDescription(newProjectDescription);

            Assert.NotNull(result);
            Assert.Equal(newProjectDescription, result.ProjectDescription);
        }
    }
}