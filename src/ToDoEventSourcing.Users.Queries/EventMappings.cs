﻿using static ToDoEventSourcing.Lib.EventSourcing.TypeMapper;
using static ToDoEventSourcing.Users.Domain.Events;

namespace ToDoEventSourcing.Users.Queries
{
    public static class EventMappings
    {
        public static void MapEventTypes()
        {
            Map<V1.UserCreated>("UserCreated");
            Map<V1.UserFirstNameChanged>("UserFirstNameChanged");
            Map<V1.UserLastNameChanged>("UserLastNameChanged");
            Map<V1.UserDisplayNameChanged>("UserDisplayNameChanged");
        }
    }
}
