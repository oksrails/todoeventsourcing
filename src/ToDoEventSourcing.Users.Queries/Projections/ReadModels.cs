﻿namespace ToDoEventSourcing.Users.Queries.Projections
{
    public class ReadModels
    {
        public class UserDetails
        {
            public string Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string DisplayName { set; get; }

            public static string GetDatabaseId(Guid id) => $"UserDetails/{id}";
        }
    }
}
