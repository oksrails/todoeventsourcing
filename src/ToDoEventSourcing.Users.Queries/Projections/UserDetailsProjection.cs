﻿using Raven.Client.Documents.Session;
using ToDoEventSourcing.Lib.RavenDb;
using static ToDoEventSourcing.Users.Domain.Events;

namespace ToDoEventSourcing.Users.Queries.Projections
{
    public static class UserDetailsProjection
    {
        public static Func<Task> GetHandler(
            IAsyncDocumentSession session,
            object @event)
        {
            Func<Guid, string> getDbId = ReadModels.UserDetails.GetDatabaseId;

#pragma warning disable CS8603 // Possible null reference return.
            return @event switch
            {
                V1.UserCreated e =>
                    () => Create(e.UserProfileId, e.FirstName, e.LastName, e.DisplayName),
                V1.UserFirstNameChanged e =>
                    () => Update(e.UserProfileId, u => u.FirstName = e.FirstName),
                V1.UserLastNameChanged e =>
                    () => Update(e.UserProfileId, u => u.LastName = e.LastName),
                V1.UserDisplayNameChanged e =>
                    () => Update(e.UserProfileId, u => u.DisplayName = e.DisplayName),
                _ => (Func<Task>)null
            };
#pragma warning restore CS8603 // Possible null reference return.

            string GetDbId(Guid id) => ReadModels.UserDetails.GetDatabaseId(id);

            Task Create(Guid id, string firstName, string lastName, string displayName)
                => session.Create<ReadModels.UserDetails>(
                    u =>
                    {
                        u.Id = GetDbId(id);
                        u.FirstName = firstName;
                        u.LastName = lastName;
                        u.DisplayName = displayName;
                    });

            Task Update(Guid id, Action<ReadModels.UserDetails> update)
                => session.Update(getDbId(id), update);
        }
    }
}
