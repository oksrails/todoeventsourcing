﻿using Raven.Client.Documents;
using static ToDoEventSourcing.Users.Queries.Projections.ReadModels;

namespace ToDoEventSourcing.Users.Queries
{
    public static class Queries
    {
        public static async Task<UserDetails> GetUserDetails(
            this GetUsersModuleSession getSession,
            Guid id)
        {
            using var session = getSession();

            return await session.LoadAsync<UserDetails>(UserDetails.GetDatabaseId(id));
        }

        public static async Task<List<UserDetails>> GetUsers(
            this GetUsersModuleSession getSession)
        {
            using var session = getSession();

            return await session.Query<UserDetails>().ToListAsync();
        }
    }
}
