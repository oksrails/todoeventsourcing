﻿namespace ToDoEventSourcing.Users.Queries
{
    public class QueryModels
    {
        public record GetUserDetailsQuery
        {
            public Guid UserId { get; set; }
        }
    }
}