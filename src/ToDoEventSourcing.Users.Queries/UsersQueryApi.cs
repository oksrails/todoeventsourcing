﻿using Microsoft.AspNetCore.Mvc;
using ToDoEventSourcing.Users.Queries.Projections;

namespace ToDoEventSourcing.Users.Queries
{
    [Route("/users")]
    public class UsersQueryApi : ControllerBase
    {
        readonly GetUsersModuleSession _getSession;

        public UsersQueryApi(GetUsersModuleSession getSession) => _getSession = getSession;

        [HttpGet("/userProfile")]
        public async Task<ActionResult<ReadModels.UserDetails>> Get([FromQuery] QueryModels.GetUserDetailsQuery request)
        {
            var user = await _getSession.GetUserDetails(request.UserId);

            if (user == null)
                return NotFound();

            return Ok(user);
        }

        [HttpGet]
        public async Task<ActionResult<List<ReadModels.UserDetails>>> Get()
        {
            var users = await _getSession.GetUsers();

            if (users == null)
                return NotFound();

            return Ok(users);
        }
    }
}
