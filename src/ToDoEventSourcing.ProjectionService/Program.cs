using EventStore.ClientAPI;
using Raven.Client.Documents;
using Serilog;
using ToDoEventSourcing.Lib.EventStore;
using ToDoEventSourcing.Users;
using ToDoEventSourcing.Projects;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((hostContext, services) =>
    {
        Log.Logger = new LoggerConfiguration()
            .Enrich.FromLogContext()
            .ReadFrom.Configuration(hostContext.Configuration)
            .CreateLogger();

        var esConnectionSettings = ConnectionSettings
            .Create()
            .KeepReconnecting()
            .EnableVerboseLogging()
            .SetReconnectionDelayTo(TimeSpan.FromMilliseconds(1000))
            .SetHeartbeatInterval(TimeSpan.FromMilliseconds(1000))
            .SetHeartbeatTimeout(TimeSpan.FromMilliseconds(3000))
            .FailOnNoServerResponse()
            .SetCompatibilityMode("auto")
            .SetOperationTimeoutTo(TimeSpan.FromMilliseconds(5000))
            .DisableServerCertificateValidation()
            .PerformOnAnyNode()
            .WithConnectionTimeoutOf(TimeSpan.FromMilliseconds(5000))
            .DisableTls()
            .Build();

        var esConnection = EventStoreConnection.Create(
            esConnectionSettings,
            new Uri(hostContext.Configuration["EventStore:ConnectionString"])
        );

        var documentStore = ConfigureRavenDb(hostContext.Configuration["ravenDb:server"]);

        services.AddLogging(configure => configure.AddSerilog(dispose: true))
            .AddSingleton<EventStoreService>();

        services.AddSingleton(documentStore);
        services.AddSingleton(esConnection);

        services.AddUserProjectionModule("Users");
        services.AddProjectProjectionModule("Projects");

        services.AddHostedService<EventStoreService>();
    })
    .Build();

await host.RunAsync();

static IDocumentStore ConfigureRavenDb(string serverUrl)
{
    var store = new DocumentStore
    {
        Urls = new[] { serverUrl }
    };
    store.Initialize();

    return store;
}
