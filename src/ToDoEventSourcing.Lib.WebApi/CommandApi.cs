﻿using Microsoft.AspNetCore.Mvc;
using ToDoEventSourcing.Lib.Common;
using ToDoEventSourcing.Lib.EventSourcing;

namespace ToDoEventSourcing.Lib.WebApi
{
    public abstract class CommandApi<T> : ControllerBase
        where T : AggregateRoot
    {
        ApplicationService<T> Service { get; }

        protected CommandApi(ApplicationService<T> applicationService)
        {
            Service = applicationService;
        }

        protected async Task<IActionResult> HandleCommand<TCommand>(
            TCommand command,
            Action<TCommand> commandModifier = null)
        {
            try
            {
                commandModifier?.Invoke(command);
                await Service.Handle(command);
                return new OkResult();
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(
                    new
                    {
                        error = ex.Message,
                        stackTrace = ex.StackTrace
                    });
            }
        }

        protected Guid GetUserId() => Guid.Parse(User.Identity.Name);
    }
}