﻿using Microsoft.AspNetCore.Mvc;

namespace ToDoEventSourcing.Lib.WebApi
{
    public static class ControllerBaseExtensions
    {
        public static async Task<ActionResult> HandleCommand(
            this ControllerBase _, 
            Task handler)
        {
            try
            {
                await handler;
                return new OkResult();
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(
                    new
                    {
                        error = ex.Message,
                        stackTrace = ex.StackTrace
                    });
            }
        }
    }
}
