﻿using ToDoEventSourcing.Lib.Common;
using ToDoEventSourcing.Lib.EventSourcing;

namespace ToDoEventSourcing.Lib.EventStore
{
    public class EsAggregateStore : IAggregateStore
    {
        readonly IEventStore _store;

        public EsAggregateStore(IEventStore store) => _store = store;

        static string GetStreamName<T>(AggregateId<T> aggregateId) 
            where T : AggregateRoot 
            => $"{typeof(T).Name}-{aggregateId.ToString("N")}";

        static string GetStreamName<T>(T aggregate)
            where T : AggregateRoot
            => StreamName.For<T>(aggregate.Id);

        public Task<bool> Exists<T>(AggregateId<T> aggregateId)
            where T : AggregateRoot
            => _store.StreamExists(GetStreamName(aggregateId));

        public async Task Save<T>(T aggregate) 
            where T : AggregateRoot
        {
            if (aggregate == null)
                throw new ArgumentNullException(nameof(aggregate));

            var stramName = GetStreamName(aggregate);
            var changes = aggregate.GetChanges().ToArray();

            await _store.AppendEvents(stramName, aggregate.Version, changes);
            
            aggregate.ClearChanges();
        }

        public async Task<T> Load<T>(AggregateId<T> aggregateId)
            where T : AggregateRoot
        {
            if (aggregateId == null)
                throw new ArgumentException(nameof(aggregateId));

            var streamName = GetStreamName(aggregateId);
            var aggregate = (T) Activator.CreateInstance(typeof(T), true);

            var events = await _store.LoadEvents(streamName);

            aggregate.Load(events);
            return aggregate;
        }
    }
}
