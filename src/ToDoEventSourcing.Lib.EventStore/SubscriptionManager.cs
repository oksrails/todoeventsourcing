﻿using EventStore.ClientAPI;
using Serilog;
using ToDoEventSourcing.Lib.EventSourcing;

namespace ToDoEventSourcing.Lib.EventStore
{
    public class SubscriptionManager
    {
        readonly Serilog.ILogger _logger;
        readonly ICheckpointStore _checkpointStore;
        readonly string _name;
        readonly StreamName _streamName;
        readonly IEventStoreConnection _connection;
        readonly ISubscription[] _subscriptions;
        EventStoreCatchUpSubscription _subscription;
        bool _isAllStream;

        public SubscriptionManager(
            ICheckpointStore checkpointStore,
            IEventStoreConnection connection,
            string name,
            StreamName streamName,
            params ISubscription[] subscriptions
            )
        {
            _checkpointStore = checkpointStore;
            _connection = connection;
            _name = name;
            _streamName = streamName;
            _isAllStream = streamName.IsAllStream;
            _subscriptions = subscriptions;
            _logger = Log.ForContext<SubscriptionManager>();
        }

        public async Task Start()
        {
            var settings = new CatchUpSubscriptionSettings(
                2000, 500, true, false, _name);

            _logger.Information("Starting the projection manager...");

            var position = await _checkpointStore.GetCheckpoint();

            _logger.Information("Retrieved the checkpoint: {checkpoint}", position);

            _subscription = _isAllStream
                ? (EventStoreCatchUpSubscription)
                _connection.SubscribeToAllFrom(GetAllStreamPosition(), settings, EventAppeared)
                : _connection.SubscribeToStreamFrom(_streamName, GetStreamPosition(), settings, EventAppeared);

            _logger.Information("Subscribed to $all stream");

            Position ? GetAllStreamPosition ()
                 => position.HasValue
                     ? new Position(position.Value, position.Value)
                     : AllCheckpoint.AllStart;

            long? GetStreamPosition()
                => position ?? StreamCheckpoint.StreamStart;
        }

        async Task EventAppeared(
            EventStoreCatchUpSubscription _,
            ResolvedEvent resolvedEvent
        )
        {
            if (resolvedEvent.Event.EventType.StartsWith("$")) return;

            try
            {
                var @event = resolvedEvent.Deserialize();

                _logger.Information("Projecting event {event}", @event.ToString());

                await Task.WhenAll(
                    _subscriptions.Select(x => x.Project(@event))
                );

                await _checkpointStore.StoreCheckpoint(
                    _isAllStream
                    ? resolvedEvent.OriginalPosition.Value.CommitPosition
                    : resolvedEvent.Event.EventNumber
                );
            }
            catch (Exception e)
            {
                _logger.Error(e, $"Error occured when projecting the event");
            }
        }

        public void Stop() => _subscription.Stop();
    }
}
