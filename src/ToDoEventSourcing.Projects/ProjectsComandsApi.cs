﻿using Microsoft.AspNetCore.Mvc;
using ToDoEventSourcing.Lib.WebApi;
using ToDoEventSourcing.Projects.Domain;
using static ToDoEventSourcing.Projects.Commands;

namespace ToDoEventSourcing.Projects
{
    [Route("/project")]
    public class ProjectsComandsApi : CommandApi<Project>
    {
        public ProjectsComandsApi(ProjectCommandService commandService)
            : base(commandService) { }

        [HttpPost]
        public Task<IActionResult> Post(V1.CreateProject command)
            => HandleCommand(command);

        [Route("title"), HttpPut]
        public Task<IActionResult> Put(V1.ChangeProjectTitle command)
            => HandleCommand(command);

        [Route("description"), HttpPut]
        public Task<IActionResult> Put(V1.ChangeProjectDescription command)
            => HandleCommand(command);
    }
}
