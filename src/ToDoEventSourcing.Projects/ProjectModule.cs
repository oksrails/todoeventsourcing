﻿using EventStore.ClientAPI;
using Microsoft.Extensions.DependencyInjection;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using ToDoEventSourcing.Lib.EventSourcing;
using ToDoEventSourcing.Lib.EventStore;
using ToDoEventSourcing.Lib.RavenDb;
using ToDoEventSourcing.Projects.Queries.Projections;
using static ToDoEventSourcing.Projects.Queries.Projections.ReadModels;

namespace ToDoEventSourcing.Projects
{
    public static class ProjectModule
    {
        const string SubscriptionName = "projectSubscription";

        public static IServiceCollection AddEsProjectsModule(this IServiceCollection services)
        {
            EventMappings.MapEventTypes();

            services.AddSingleton(s => new ProjectCommandService(s.GetAggregateStore()));

            return services;
        }

        public static IServiceCollection AddProjectProjectionModule(
            this IServiceCollection services,
            string databaseName)
        {
            EventMappings.MapEventTypes();

            services
                .AddSingleton<GetProjectsModuleSession>(
                    c =>
                    {
                        var store = c.GetRequiredService<IDocumentStore>();
                        store.CheckAndCreateDatabase(databaseName);

                        IAsyncDocumentSession GetSession()
                            => store.OpenAsyncSession(databaseName);

                        return GetSession;
                    }
                )
                .AddSingleton(
                    c =>
                    {
                        var getSession =
                            c.GetRequiredService<GetProjectsModuleSession>();

                        return new SubscriptionManager(
                            new RavenDbCheckpointStore(
                                () => getSession(),
                                SubscriptionName
                            ),
                            c.GetEsConnection(),
                            SubscriptionName,
                            StreamName.AllStream,
                            new RavenDbProjection<ProjectDetails>(
                                () => getSession(),
                                ProjectDetailsProjection.GetHanlder
                            )
                        );
                    }
                );

            return services;
        }

        static IDocumentStore GetRavenStore(
            this IServiceProvider provider
        )
            => provider.GetRequiredService<IDocumentStore>();

        static IEventStoreConnection GetEsConnection(this IServiceProvider provider)
            => provider.GetRequiredService<IEventStoreConnection>();

        static IAggregateStore GetAggregateStore(this IServiceProvider provider)
            => provider.GetRequiredService<IAggregateStore>();
    }

    public delegate IAsyncDocumentSession GetProjectsModuleSession();
}
