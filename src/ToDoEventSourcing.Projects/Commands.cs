﻿namespace ToDoEventSourcing.Projects
{
    public class Commands
    {
        public static class V1
        {
            public record CreateProject
            {
                public Guid Id { get; init; }
                public Guid ProjectOwnerId { get; init; }
            }

            public record ChangeProjectTitle
            {
                public Guid Id { get; init; }
                public string Title { get; init; }
            }

            public record ChangeProjectDescription
            {
                public Guid Id { get; init; }
                public string Description { get; init; }
            }
        }
    }
}