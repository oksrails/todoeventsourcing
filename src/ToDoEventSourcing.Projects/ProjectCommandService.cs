﻿using ToDoEventSourcing.Lib.EventSourcing;
using ToDoEventSourcing.Projects.Domain;
using static ToDoEventSourcing.Projects.Commands;

namespace ToDoEventSourcing.Projects
{
    public class ProjectCommandService : ApplicationService<Project>
    {
        public ProjectCommandService(IAggregateStore store)
            : base(store)
        {
            CreateWhen<V1.CreateProject>(
                cmd => ProjectId.FromGuid(cmd.Id),
                (cmd, id) => Project.CreateProject(
                    ProjectId.FromGuid(id),
                    UserId.FromGuid(cmd.ProjectOwnerId)));

            UpdateWhen<V1.ChangeProjectTitle>(
                cmd => ProjectId.FromGuid(cmd.Id),
                (p, cmd) => p.SetProjectTitle(ProjectTitle.FromString(cmd.Title)));

            UpdateWhen<V1.ChangeProjectDescription>(
                cmd => ProjectId.FromGuid(cmd.Id),
                (p, cmd) => p.SetProjectDescription(ProjectDescription.FromString(cmd.Description)));
        }
    }
}
