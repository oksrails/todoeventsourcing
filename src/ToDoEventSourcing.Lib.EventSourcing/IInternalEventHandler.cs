﻿namespace ToDoEventSourcing.Lib.EventSourcing
{
    public interface IInternalEventHandler
    {
        void Handle(object @event);
    }
}
