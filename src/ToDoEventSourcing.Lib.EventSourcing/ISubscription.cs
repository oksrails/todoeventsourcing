﻿namespace ToDoEventSourcing.Lib.EventSourcing
{
    public interface ISubscription
    {
        Task Project(object @event);
    }
}
