﻿using ToDoEventSourcing.Lib.Common;

namespace ToDoEventSourcing.Lib.EventSourcing
{
    public interface IAggregateStore
    {
        Task Save<T>(T aggregate) where T : AggregateRoot;

        Task<T> Load<T>(AggregateId<T> aggregateId) where T : AggregateRoot;

        Task<bool> Exists<T>(AggregateId<T> aggregateId) where T : AggregateRoot;
    }
}
