using ToDoEventSourcing.Projects.Queries;

var builder = WebApplication.CreateBuilder(args);

AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c => c.SwaggerDoc("v1", new OpenApiInfo { Title = "ToDoEsEntityService", Version = "v0.1" }));

var esConnectionSettings = ConnectionSettings
    .Create()
    .KeepReconnecting()
    .EnableVerboseLogging()
    .SetReconnectionDelayTo(TimeSpan.FromMilliseconds(1000))
    .SetHeartbeatInterval(TimeSpan.FromMilliseconds(1000))
    .SetHeartbeatTimeout(TimeSpan.FromMilliseconds(3000))
    .FailOnNoServerResponse()
    .SetCompatibilityMode("auto")
    .SetOperationTimeoutTo(TimeSpan.FromMilliseconds(5000))
    .DisableServerCertificateValidation()
    .PerformOnAnyNode()
    .UseConsoleLogger()
    .WithConnectionTimeoutOf(TimeSpan.FromMilliseconds(5000))
    .DisableTls()
    .Build();

var esConnection = EventStoreConnection.Create(
    esConnectionSettings,
    new Uri(builder.Configuration["EventStore:ConnectionString"])
);
var eventStore = new ToDoEventSourcing.Lib.EventStore.EventStore(esConnection);

builder.Services.AddMvcCore(options => options.Conventions.Add(new CommandConvetion()));

builder.Services.AddSingleton(esConnection);
builder.Services.AddSingleton<IEventStore>(eventStore);

builder.Services.AddSingleton<IAggregateStore>(new EsAggregateStore(eventStore));

builder.Services.AddEsProjectsModule();
builder.Services.AddEsUsersModule();
builder.Services.AddTasksModule();

await esConnection.ConnectAsync();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(
        c => c.SwaggerEndpoint(
            "/swagger/v1/swagger.json",
            "ToDoEsEntityService v0.1"
            )
        );
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();