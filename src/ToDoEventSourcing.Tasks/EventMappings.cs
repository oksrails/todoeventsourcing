﻿using static ToDoEventSourcing.Lib.EventSourcing.TypeMapper;
using static ToDoEventSourcing.ProjectTasks.Domain.Events;

namespace ToDoEventSourcing.ProjectTasks
{
    internal static class EventMappings
    {
        public static void MapEventTypes()
        {
            Map<V1.ProjectTaskCreated>("ProjectTaskCreated");
            Map<V1.ProjectTaskTitleChanged>("ProjectTaskTitleChanged");
            Map<V1.ProjectTaskDescriptionChanged>("ProjectTaskDescriptionChanged");
            Map<V1.ProjectTaskTypeChanged>("ProjectTaskTypeChanged");
            Map<V1.ProjectTaskEstimationChanged>("ProjectTaskEstimationChanged");
            Map<V1.ProjectTaskAssigned>("ProjectTaskAssigned");
            Map<V1.ProjectTaskWorkStarted>("ProjectTaskWorkStarted");
            Map<V1.ProjectTaskComplited>("ProjectTaskComplited");
        }
    }
}
