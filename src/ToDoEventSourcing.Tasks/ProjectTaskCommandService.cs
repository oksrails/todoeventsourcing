﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoEventSourcing.Lib.EventSourcing;
using ToDoEventSourcing.ProjectTasks.Domain;
using static ToDoEventSourcing.ProjectTasks.Commands;

namespace ToDoEventSourcing.ProjectTasks
{
    public class ProjectTaskCommandService : ApplicationService<ProjectTask>
    {
        public ProjectTaskCommandService(IAggregateStore store)
            : base(store)
        {
            CreateWhen<V1.Create>(
                cmd => ProjectTaskId.FromGuid(cmd.Id),
                (cmd, id) => ProjectTask.Create(
                    ProjectTaskId.FromGuid(id),
                    ProjectId.FromGuid(cmd.ProjectId)));

            UpdateWhen<V1.ChangeTitle>(
                cmd => ProjectTaskId.FromGuid(cmd.Id),
                (t, cmd) => t.SetProjectTaskTitle(TaskTitle.FromString(cmd.TaskTitle)));

            UpdateWhen<V1.ChangeDescription>(
                cmd => ProjectTaskId.FromGuid(cmd.Id),
                (t, cmd) => t.SetProjectTaskDescription(TaskDescription.FromString(cmd.TaskDescription)));

            UpdateWhen<V1.ChangeType>(
                cmd => ProjectTaskId.FromGuid(cmd.Id),
                (t, cmd) => t.SetProjectTaskType(cmd.TaskType));

            UpdateWhen<V1.ChangeEstimation>(
                cmd => ProjectTaskId.FromGuid(cmd.Id),
                (t, cmd) => t.SetProjectTaskEstimation(TaskEstimation.FromDecimal(cmd.TaskEstimation)));

            UpdateWhen<V1.AssignTask>(
                cmd => ProjectTaskId.FromGuid(cmd.Id),
                (t, cmd) => t.AssignProjectTask(UserId.FromGuid(cmd.AssignedUserId)));

            UpdateWhen<V1.StartWork>(
                cmd => ProjectTaskId.FromGuid(cmd.Id),
                (t, cmd) => t.StartWorkOnProjectTask());

            UpdateWhen<V1.CompliteTask>(
                cmd => ProjectTaskId.FromGuid(cmd.Id),
                (t, cmd) => t.CompleteProjectTask());
        }
    }
}
