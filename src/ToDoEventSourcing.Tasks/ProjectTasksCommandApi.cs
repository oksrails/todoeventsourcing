﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDoEventSourcing.Lib.WebApi;
using ToDoEventSourcing.ProjectTasks.Domain;
using static ToDoEventSourcing.ProjectTasks.Commands;

namespace ToDoEventSourcing.ProjectTasks
{
    [Route("/projectTasks")]
    public class ProjectTasksCommandApi : CommandApi<ProjectTask>
    {
        public ProjectTasksCommandApi(ProjectTaskCommandService commandService)
            : base(commandService) { }

        [HttpPost]
        public Task<IActionResult> Post(V1.Create command)
            => HandleCommand(command);

        [Route("title"), HttpPut]
        public Task<IActionResult> Put(V1.ChangeTitle command)
            => HandleCommand(command);

        [Route("description"), HttpPut]
        public Task<IActionResult> Put(V1.ChangeDescription command)
            => HandleCommand(command);

        [Route("estimation"), HttpPut]
        public Task<IActionResult> Put(V1.ChangeEstimation command)
            => HandleCommand(command);

        [Route("taskType"), HttpPut]
        public Task<IActionResult> Put(V1.ChangeType command)
            => HandleCommand(command);

        [Route("assign"), HttpPut]
        public Task<IActionResult> Put(V1.AssignTask command)
            => HandleCommand(command);

        [Route("start"), HttpPut]
        public Task<IActionResult> Put(V1.StartWork command)
            => HandleCommand(command);

        [Route("complite"), HttpPut]
        public Task<IActionResult> Put(V1.CompliteTask command)
            => HandleCommand(command);
    }
}
