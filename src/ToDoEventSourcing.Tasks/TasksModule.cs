﻿using EventStore.ClientAPI;
using Microsoft.Extensions.DependencyInjection;
using ToDoEventSourcing.Lib.EventSourcing;

namespace ToDoEventSourcing.ProjectTasks
{
    public static class TasksModule
    {
        public static IServiceCollection AddTasksModule(this IServiceCollection services)
        {
            EventMappings.MapEventTypes();

            services.AddSingleton(s =>  new ProjectTaskCommandService(s.GetAggregateStore()));

            return services;
        }

        static IEventStoreConnection GetEsConnection(this IServiceProvider provider)
            => provider.GetRequiredService<IEventStoreConnection>();

        static IAggregateStore GetAggregateStore(this IServiceProvider provider)
            => provider.GetRequiredService<IAggregateStore>();
    }
}
