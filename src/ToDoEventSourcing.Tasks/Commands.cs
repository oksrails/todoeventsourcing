﻿using ToDoEventSourcing.ProjectTasks.Domain;

namespace ToDoEventSourcing.ProjectTasks
{
    public static class Commands
    {
        public static class V1
        {
            public record Create
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
            }

            public record ChangeTitle
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
                public string TaskTitle { get; init; }
            }

            public record ChangeDescription
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
                public string TaskDescription { get; init; }
            }

            public record ChangeType
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
                public TaskType TaskType { get; init; }
            }

            public record ChangeEstimation
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
                public decimal TaskEstimation { get; init; }
            }

            public record AssignTask
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
                public Guid AssignedUserId { get; init; }
            }

            public record StartWork
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
            }

            public record CompliteTask
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
            }
        }
    }
}