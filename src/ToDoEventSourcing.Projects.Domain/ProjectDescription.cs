﻿using ToDoEventSourcing.Lib.Common;
using static System.String;

namespace ToDoEventSourcing.Projects.Domain
{
    public class ProjectDescription : Value<ProjectDescription>
    {
        public string Value { get; }

        protected ProjectDescription(string value) => Value = value;

        public static ProjectDescription FromString(string value)
        {
            CheckValidity(value);
            return new ProjectDescription(value);
        }

        public static implicit operator string(ProjectDescription value) => value.Value;

        private static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new ProjectDomainException("Description cannot be empty");

            if (value.Length < 10)
                throw new ProjectDomainException("Description cannot be shorter than 10 characters");
        }
    }
}
