﻿using ToDoEventSourcing.Lib.Common;
using static ToDoEventSourcing.Projects.Domain.Events;

namespace ToDoEventSourcing.Projects.Domain
{
    public class Project : AggregateRoot
    {
        ProjectTitle ProjectTitle { get; set; }
        ProjectDescription ProjectDescription { get; set; }
        UserId ProjectOwnerId { get; set; }

        public static Project CreateProject(
            ProjectId id,
            UserId projectOwner)
        {
            var project = new Project();

            var projectCreated = new V1.ProjectCreated
            {
                Id = id,
                ProjectOwnerId = projectOwner
            };

            project.Apply(projectCreated);
            return project;
        }

        public V1.ProjectTitleChanged SetProjectTitle(ProjectTitle projectTitle)
        {
            var projectTitleChanged = new V1.ProjectTitleChanged
            {
                Id = Id,
                ProjectOwnerId = ProjectOwnerId,
                ProjectTitle = projectTitle
            };

            Apply(projectTitleChanged);
            return projectTitleChanged;
        }

        public V1.ProjectDescriptionChanged SetProjectDescription(ProjectDescription projectDescription)
        {
            var projectDescriptionChanged = new V1.ProjectDescriptionChanged
            {
                Id = Id,
                ProjectOwnerId = ProjectOwnerId,
                ProjectDescription = projectDescription
            };

            Apply(projectDescriptionChanged);
            return projectDescriptionChanged;
        }

        protected override void EnsureValidState()
        {
            
        }

        private void EnsureDoesNotExist()
        {
            if (Version > -1)
                throw new ProjectDomainException("Project allready exists");
        }

        protected override void When(object evt)
        {
            switch (evt)
            {
                case V1.ProjectCreated e:
                    {
                        Id = e.Id;
                        ProjectOwnerId = UserId.FromGuid(e.ProjectOwnerId);
                        break;
                    }
                case V1.ProjectTitleChanged e:
                    {
                        Id = e.Id;
                        ProjectTitle = ProjectTitle.FromString(e.ProjectTitle);
                        ProjectOwnerId = UserId.FromGuid(e.ProjectOwnerId);
                        break;
                    }
                case V1.ProjectDescriptionChanged e:
                    {
                        Id = e.Id;
                        ProjectOwnerId = UserId.FromGuid(e.ProjectOwnerId);
                        ProjectDescription = ProjectDescription.FromString(e.ProjectDescription);
                        break;
                    }
            }
        }
    }
}