﻿namespace ToDoEventSourcing.Projects.Domain
{
    public static class Events
    {
        public static class V1
        {
            public record ProjectCreated
            {
                public Guid Id { get; init; }
                public Guid ProjectOwnerId { get; init; }

                public override string ToString() => $"{nameof(ProjectCreated)}";
            }

            public record ProjectTitleChanged
            {
                public Guid Id { get; init; }
                public string ProjectTitle { get; init; }
                public Guid ProjectOwnerId { get; init; }

                public override string ToString() => $"{nameof(ProjectTitleChanged)}";
            }

            public record ProjectDescriptionChanged
            {
                public Guid Id { get; init; }
                public string ProjectDescription { get; init; }
                public Guid ProjectOwnerId { get; init; }

                public override string ToString() => $"{nameof(ProjectDescriptionChanged)}";
            }
        }
    }
}
