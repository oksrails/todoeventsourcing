﻿using ToDoEventSourcing.Lib.Common;
using static System.String;

namespace ToDoEventSourcing.Projects.Domain
{
    public class ProjectTitle : Value<ProjectTitle>
    {
        public string Value { get; }

        protected ProjectTitle() { }

        internal ProjectTitle(string value) => Value = value;

        public static ProjectTitle FromString(string value)
        {
            CheckValidity(value);
            return new ProjectTitle(value);
        }

        public static implicit operator string(ProjectTitle value) => value.Value;

        private static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new ProjectDomainException("Title cannot be empty");

            if (value.Length < 10)
                throw new ProjectDomainException("Title cannot be shorter than 10 characters");

            if (value.Length > 100)
                throw new ProjectDomainException("Title cannot be longer than 100 characters");
        }
    }
}
