﻿using ToDoEventSourcing.Lib.Common;

namespace ToDoEventSourcing.Projects.Domain
{
    public class ProjectDomainException : DomainBaseException
    {
        public ProjectDomainException(string exceptionCode) : base(exceptionCode) { }
    }
}