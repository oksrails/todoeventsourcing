﻿using ToDoEventSourcing.Lib.Common;

namespace ToDoEventSourcing.Projects.Domain
{
    public class ProjectId : AggregateId<Project>
    {
        ProjectId(Guid value) : base(value) { }

        public static ProjectId FromGuid(Guid value) => new ProjectId(value);
    }
}
