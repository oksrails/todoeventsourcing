﻿using Raven.Client.Documents.Session;

namespace ToDoEventSourcing.Lib.RavenDb
{
    public delegate IAsyncDocumentSession GetSession();
}
