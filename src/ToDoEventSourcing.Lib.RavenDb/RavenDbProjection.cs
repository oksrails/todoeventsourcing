﻿using Raven.Client.Documents.Session;
using ToDoEventSourcing.Lib.EventSourcing;

namespace ToDoEventSourcing.Lib.RavenDb
{
    public class RavenDbProjection<T> : ISubscription
    {
        public delegate Func<Task> Projector(IAsyncDocumentSession session, object @event);

        static readonly string ReadModelName = typeof(T).Name;

        GetSession GetSession { get; }
        readonly Projector _projector;

        public RavenDbProjection(
            GetSession getSession,
            Projector projector
            )
        {
            GetSession = getSession;
            _projector = projector;
        }

        public async Task Project(object @event)
        {
            using var session = GetSession();

            var handler = _projector(session, @event);

            if (handler == null)
                return;

            await handler();
            await session.SaveChangesAsync();
        }
    }
}
