using Xunit;
using System;

namespace ToDoEventSourcing.Users.Domain.Tests
{
    public class UserDomainTest
    {
        UserProfile _userProfile;

        public UserDomainTest()
        {
            _userProfile = UserProfile.Create(
                UserProfileId.FromGuid(Guid.NewGuid()),
                FirstName.FromString("First"),
                LastName.FromString("Last"),
                DisplayName.FromString("Display"));
        }

        [Fact]
        public void Change_first_name_succes_test()
        {
            var newFirstNameInput = "New";

            var firstNameChanged = _userProfile.ChangeFirstName(FirstName.FromString(newFirstNameInput));

            Assert.NotNull(firstNameChanged);
            Assert.Equal(newFirstNameInput, firstNameChanged.FirstName);
        }

        [Fact]
        public void Change_last_name_success_test()
        {
            var newLastNameInput = "New";

            var lastNameChanged = _userProfile.ChangeLastName(LastName.FromString(newLastNameInput));

            Assert.NotNull(lastNameChanged);
            Assert.Equal(newLastNameInput, lastNameChanged.LastName);
        }

        [Fact]
        public void Change_display_name_success_test()
        {
            var newDisplayNameInput = "New";

            var displayNameChanged = _userProfile.ChangeDisplayName(DisplayName.FromString(newDisplayNameInput));

            Assert.NotNull(displayNameChanged);
            Assert.Equal(newDisplayNameInput, displayNameChanged.DisplayName);
        }
    }
}