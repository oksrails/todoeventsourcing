﻿using EventStore.ClientAPI;
using Microsoft.Extensions.DependencyInjection;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using ToDoEventSourcing.Lib.EventSourcing;
using ToDoEventSourcing.Lib.EventStore;
using ToDoEventSourcing.Lib.RavenDb;
using ToDoEventSourcing.Users.Queries.Projections;
using static ToDoEventSourcing.Users.Queries.Projections.ReadModels;

namespace ToDoEventSourcing.Users
{
    public static class UsersModule
    {
        const string SubscriptionName = "usersSubscription";

        public static IServiceCollection AddEsUsersModule(this IServiceCollection services)
        {
            EventMappings.MapEventTypes();

            services.AddSingleton(s => new UserCommandService(s.GetAggregateStore()));

            return services;
        }

        public static IServiceCollection AddUserProjectionModule (
            this IServiceCollection services,
            string databaseName)
        {
            EventMappings.MapEventTypes();

            services
                .AddSingleton<GetUsersModuleSession>(
                    c =>
                    {
                        var store = c.GetRequiredService<IDocumentStore>();
                        store.CheckAndCreateDatabase(databaseName);

                        IAsyncDocumentSession GetSession()
                            => store.OpenAsyncSession(databaseName);

                        return GetSession;
                    }
                )
                .AddSingleton(
                    c =>
                    {
                        var getSession =
                            c.GetRequiredService<GetUsersModuleSession>();

                        return new SubscriptionManager(
                            new RavenDbCheckpointStore(
                                () => getSession(),
                                SubscriptionName
                            ),
                            c.GetEsConnection(),
                            SubscriptionName,
                            StreamName.AllStream,
                            new RavenDbProjection<UserDetails>(
                                () => getSession(),
                                UserDetailsProjection.GetHandler
                            )
                        );
                    }
                );

            return services;
        }

        static IEventStoreConnection GetEsConnection(this IServiceProvider provider)
            => provider.GetRequiredService<IEventStoreConnection>();

        static IAggregateStore GetAggregateStore(this IServiceProvider provider)
            => provider.GetRequiredService<IAggregateStore>();
    }

    public delegate IAsyncDocumentSession GetUsersModuleSession();
}
