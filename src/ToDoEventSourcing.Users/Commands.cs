﻿namespace ToDoEventSourcing.Users
{
    public static class Commands
    {
        public static class V1
        {
            public record CreateUser
            {
                public Guid UserProfileId { get; init; }
                public string FirstName { get; init; }
                public string LastName { get; init; }
                public string DisplayName { get; init; }
            }

            public record ChangeFirstName
            {
                public Guid UserProfileId { get; init; }
                public string FirstName { get; init; }
            }

            public record ChangeLastName
            {
                public Guid UserProfileId { get; init; }
                public string LastName { get; init; }
            }

            public record ChangeDisplayName
            {
                public Guid UserProfileId { get; init; }
                public string DisplayName { get; init; }
            }
        }
    }
}