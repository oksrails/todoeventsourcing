﻿using ToDoEventSourcing.Lib.EventSourcing;
using ToDoEventSourcing.Users.Domain;
using static ToDoEventSourcing.Users.Commands;

namespace ToDoEventSourcing.Users
{
    public class UserCommandService : ApplicationService<UserProfile>
    {
        public UserCommandService(IAggregateStore store)
            : base(store)
        {
            CreateWhen<V1.CreateUser>(
                cmd => UserProfileId.FromGuid(cmd.UserProfileId),
                (cmd, id) => UserProfile.Create(
                    UserProfileId.FromGuid(id),
                    FirstName.FromString(cmd.FirstName),
                    LastName.FromString(cmd.LastName),
                    DisplayName.FromString(cmd.DisplayName)));

            UpdateWhen<V1.ChangeFirstName>(
                cmd => UserProfileId.FromGuid(cmd.UserProfileId),
                (u, cmd) => u.ChangeFirstName(FirstName.FromString(cmd.FirstName)));

            UpdateWhen<V1.ChangeLastName>(
                cmd => UserProfileId.FromGuid(cmd.UserProfileId),
                (u, cmd) => u.ChangeLastName(LastName.FromString(cmd.LastName)));

            UpdateWhen<V1.ChangeDisplayName>(
                cmd => UserProfileId.FromGuid(cmd.UserProfileId),
                (u, cmd) => u.ChangeDisplayName(DisplayName.FromString(cmd.DisplayName)));
        }
    }
}
