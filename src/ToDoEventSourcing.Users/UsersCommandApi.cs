﻿using Microsoft.AspNetCore.Mvc;
using ToDoEventSourcing.Lib.WebApi;
using ToDoEventSourcing.Users.Domain;
using static ToDoEventSourcing.Users.Commands;

namespace ToDoEventSourcing.Users
{
    [Route("/userProfile")]
    public class UsersCommandApi : CommandApi<UserProfile>
    {
        public UsersCommandApi(UserCommandService commandService)
            : base(commandService) { }

        [HttpPost]
        public Task<IActionResult> Post(V1.CreateUser command)
            => HandleCommand(command);

        [Route("firstName"), HttpPut]
        public Task<IActionResult> Put(V1.ChangeFirstName command)
            => HandleCommand(command);

        [Route("lastName"), HttpPut]
        public Task<IActionResult> Put(V1.ChangeLastName command)
            => HandleCommand(command);

        [Route("displayName"), HttpPut]
        public Task<IActionResult> Put(V1.ChangeDisplayName command)
            => HandleCommand(command);
    }
}
