using Microsoft.OpenApi.Models;
using Raven.Client.Documents;
using ToDoEventSourcing.Projects.Queries;
using ToDoEventSourcing.Users.Queries;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c => c.SwaggerDoc("v1", new OpenApiInfo { Title = "ToDoSelectionService", Version = "v0.1" }));

var documentStore = ConfigureRavenDb(builder.Configuration["ravenDb:server"]);
builder.Services.AddSingleton(documentStore);

builder.Services.AddUsersSelectionModule("Users");
builder.Services.AddProjectsSelectionModule("Projects");

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(
        c => c.SwaggerEndpoint(
            "/swagger/v1/swagger.json",
            "ToDoSelectionService v0.1"
            )
        );
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

static IDocumentStore ConfigureRavenDb(string serverUrl)
{
    var store = new DocumentStore
    {
        Urls = new[] { serverUrl }
    };
    store.Initialize();

    return store;
}