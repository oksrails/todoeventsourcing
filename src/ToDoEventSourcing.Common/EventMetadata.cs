﻿namespace ToDoEventSourcing.Lib.Common
{
    public record EventMetadata
    {
        public string? ClrType { get; set; }
    }
}
