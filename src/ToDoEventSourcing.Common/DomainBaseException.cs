﻿using System.Runtime.Serialization;

namespace ToDoEventSourcing.Lib.Common
{
    public abstract class DomainBaseException : Exception
    {
        protected DomainBaseException(string message) : base(message) { }
    }
}
