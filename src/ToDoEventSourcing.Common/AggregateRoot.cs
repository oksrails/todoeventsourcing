﻿

namespace ToDoEventSourcing.Lib.Common
{
    public abstract class AggregateRoot : IInternalEventHandler
    {
        readonly List<object> _changes = new List<object>();

        public Guid Id { get; protected set; }

        public int Version { get; private set; } = -1;

        void IInternalEventHandler.Handle(object @event) => When(@event);

        protected void Apply(object evt)
        {
            When(evt);
            EnsureValidState();
            _changes.Add(evt);
        }

        public void Load(IEnumerable<object> events)
        {
            foreach (var @event in events)
            {
                When(@event);
                Version++;
            }
        }

        protected abstract void When(object evt);

        protected abstract void EnsureValidState();

        public IEnumerable<object> GetChanges() => _changes.AsEnumerable();

        public void ClearChanges() => _changes.Clear();

        protected void ApplyToEntity(IInternalEventHandler entity, object @event) => entity?.Handle(@event);
    }
}