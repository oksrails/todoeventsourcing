﻿namespace ToDoEventSourcing.Lib.Common
{
    public interface IInternalEventHandler
    {
        void Handle(object @event);
    }
}
