﻿using Xunit;

namespace ToDoEventSourcing.ProjectTasks.Domain.Tests
{
    public class TaskTitleTests
    {
        [Fact]
        public void Create_task_title_success()
        {
            string taskTitleInput = "Task title";

            var taskTitle = TaskTitle.FromString(taskTitleInput);

            Assert.NotNull(taskTitle);
            Assert.Equal(taskTitleInput, taskTitle);
        }

        [Fact]
        public void Create_task_title_with_empty_value_exception_test()
        {
            string taskTitleInput = "";

            Assert.Throws<ProjectTaskDomainException>(() => TaskTitle.FromString(taskTitleInput));
        }

        [Fact]
        public void Create_task_title_with_less_10_value_exception_test()
        {
            string taskTitleInput = "A";

            Assert.Throws<ProjectTaskDomainException>(() => TaskTitle.FromString(taskTitleInput));
        }

        [Fact]
        public void Create_task_title_with_more_100_value_exception_test()
        {
            var taskTitleInput = TestTools.GenerateRandomString(100);

            Assert.Throws<ProjectTaskDomainException>(() => TaskTitle.FromString(taskTitleInput));
        }
    }
}
