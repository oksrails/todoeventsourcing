﻿using Xunit;

namespace ToDoEventSourcing.ProjectTasks.Domain.Tests
{
    public class ProjectDescriptionTests
    {
        [Fact]
        public void Create_project_description_success_test()
        {
            string projectDescriptionInput = "Test project description";

            var projectDescription = TaskDescription.FromString(projectDescriptionInput);

            Assert.NotNull(projectDescription);
            Assert.Equal(projectDescriptionInput, projectDescription);
        }

        [Fact]
        public void Create_project_description_with_empty_value_exception_test()
        {
            var projectDescriptionInput = "";

            Assert.Throws<ProjectTaskDomainException>(() => TaskDescription.FromString(projectDescriptionInput));
        }

        [Fact]
        public void Create_project_description_with_less_10_value_exception_test()
        {
            var projectDescriptionInput = "A";

            Assert.Throws<ProjectTaskDomainException>(() => TaskDescription.FromString(projectDescriptionInput));
        }
    }
}
