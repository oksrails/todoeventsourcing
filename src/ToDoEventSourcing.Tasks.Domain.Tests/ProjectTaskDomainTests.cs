using System;
using Xunit;

namespace ToDoEventSourcing.ProjectTasks.Domain.Tests
{
    public class ProjectTaskDomainTests
    {
        ProjectTask _task;

        public ProjectTaskDomainTests()
        {
            _task = ProjectTask.Create(
                ProjectTaskId.FromGuid(Guid.NewGuid()),
                ProjectId.FromGuid(Guid.NewGuid()));
        }

        [Fact]
        public void Change_task_title_success_test()
        {
            var taskTitleNew = TaskTitle.FromString("New test task title");

            var taskTitleChanged = _task.SetProjectTaskTitle(taskTitleNew);

            Assert.Equal(taskTitleNew, taskTitleChanged.TaskTitle);
        }

        [Fact]
        public void Change_task_description_success_test()
        {
            var taskDescriptionNew = TaskDescription.FromString("New test task description");

            var taskDescriptionChanged = _task.SetProjectTaskDescription(taskDescriptionNew);

            Assert.Equal(taskDescriptionNew, taskDescriptionChanged.TaskDescription);
        }

        [Fact]
        public void Change_task_type_success_test()
        {
            var taskType = TaskType.Epic;

            var taskTypeChanged = _task.SetProjectTaskType(taskType);

            Assert.Equal(taskType, taskTypeChanged.TaskType);
        }

        [Fact]
        public void Change_task_estimation_success_test()
        {
            TaskEstimation estimation = TaskEstimation.FromDecimal(13.5m);

            var taskEstimationChanged = _task.SetProjectTaskEstimation(estimation);

            Assert.Equal(estimation.Value, taskEstimationChanged.TaskEstimation);
        }

        [Fact]
        public void Assign_project_task_success_test()
        {
            UserId userId = UserId.FromGuid(Guid.NewGuid());

            var taskAssignmentChanged = _task.AssignProjectTask(userId);

            Assert.Equal(userId, taskAssignmentChanged.AssignedUserId);
        }

        [Fact]
        public void Take_project_task_to_work_success_test()
        {
            _task.StartWorkOnProjectTask();
            Assert.Equal(ProjectTask.ProjectTaskStatus.InProgress, _task.TaskStatus);
        }

        [Fact]
        public void Complete_project_task_success_test()
        {
            _task.CompleteProjectTask();
            Assert.Equal(ProjectTask.ProjectTaskStatus.Done, _task.TaskStatus);
        }
    }
}