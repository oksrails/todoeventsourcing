﻿using ToDoEventSourcing.Lib.Common;

namespace ToDoEventSourcing.ProjectTasks.Domain
{
    public class TaskEstimation : Value<TaskEstimation>
    {
        public decimal Value { get; }

        internal TaskEstimation(decimal value) => Value = value;

        public static TaskEstimation FromDecimal(decimal value) => new TaskEstimation(value);

        public static TaskEstimation FromString(string value) => new TaskEstimation(decimal.Parse(value));

        public override string ToString() => Value.ToString();
    }
}
