﻿namespace ToDoEventSourcing.ProjectTasks.Domain
{
    public enum TaskType
    {
        Epic, Feature, UserStory
    }
}
