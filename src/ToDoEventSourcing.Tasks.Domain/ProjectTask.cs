﻿using ToDoEventSourcing.Lib.Common;
using static ToDoEventSourcing.ProjectTasks.Domain.Events;

namespace ToDoEventSourcing.ProjectTasks.Domain
{
    public class ProjectTask : AggregateRoot
    {
        public enum ProjectTaskStatus
        {
            Open, InProgress, Done 
        }

        ProjectId ProjectId { get; set; }
        TaskTitle TaskTitle { get; set; }
        TaskDescription TaskDescription { get; set; }
        TaskType TaskType { get; set; }
        TaskEstimation TaskEstimation { get; set; }
        public ProjectTaskStatus TaskStatus { get; private set; }

        public static ProjectTask Create(
            ProjectTaskId id,
            ProjectId projectId)
        {
            var projectTask = new ProjectTask();

            projectTask.Apply(
                new V1.ProjectTaskCreated
                {
                    Id = id,
                    ProjectId = projectId
                });

            return projectTask;
        }

        public V1.ProjectTaskTitleChanged SetProjectTaskTitle(TaskTitle taskTitle)
        {
            var taskTitleChanged = new V1.ProjectTaskTitleChanged
            {
                Id = Id,
                TaskTitle = taskTitle,
                ProjectId = ProjectId
            };
            Apply(taskTitleChanged);
            return taskTitleChanged;
        }

        public V1.ProjectTaskDescriptionChanged SetProjectTaskDescription(TaskDescription taskDescription)
        {
            var descriptionChanged = new V1.ProjectTaskDescriptionChanged
            {
                Id= Id,
                TaskDescription = taskDescription,
                ProjectId = ProjectId
            };
            Apply(descriptionChanged);
            return descriptionChanged;
        }

        public V1.ProjectTaskTypeChanged SetProjectTaskType(TaskType taskType)
        {
            var taskTypeChanged = new V1.ProjectTaskTypeChanged
            {
                Id = Id,
                ProjectId = ProjectId,
                TaskType = taskType
            };
            Apply(taskTypeChanged);
            return taskTypeChanged;
        }

        public V1.ProjectTaskEstimationChanged SetProjectTaskEstimation(TaskEstimation taskEstimation)
        {
            var taskEstimationChanged = new V1.ProjectTaskEstimationChanged
            {
                Id = Id,
                ProjectId= ProjectId,
                TaskEstimation = taskEstimation.Value
            };
            Apply(taskEstimationChanged);
            return taskEstimationChanged;
        }

        public V1.ProjectTaskAssigned AssignProjectTask(UserId userId)
        {
            var projectTaskAssigned = new V1.ProjectTaskAssigned
            {
                Id = Id,
                ProjectId = ProjectId,
                AssignedUserId = userId
            };
            Apply(projectTaskAssigned);
            return projectTaskAssigned;
        }

        public V1.ProjectTaskWorkStarted StartWorkOnProjectTask()
        {
            var projectTaskStarted = new V1.ProjectTaskWorkStarted
            {
                Id = Id,
                ProjectId = ProjectId
            };
            Apply(projectTaskStarted);
            return projectTaskStarted;
        }

        public V1.ProjectTaskComplited CompleteProjectTask()
        {
            var projectTaskComplited = new V1.ProjectTaskComplited
            {
                Id= Id,
                ProjectId = ProjectId
            };
            Apply(projectTaskComplited);
            return projectTaskComplited;
        }

        protected override void EnsureValidState()
        {
            
        }

        protected override void When(object evt)
        {
            switch (evt)
            {
                case V1.ProjectTaskCreated e:
                    Id = e.Id;
                    ProjectId = ProjectId.FromGuid(e.ProjectId);
                    TaskStatus = ProjectTaskStatus.Open;
                    break;

                case V1.ProjectTaskTitleChanged e:
                    TaskTitle = TaskTitle.FromString(e.TaskTitle);
                    break;

                case V1.ProjectTaskDescriptionChanged e:
                    TaskDescription = TaskDescription.FromString(e.TaskDescription);
                    break;

                case V1.ProjectTaskTypeChanged e:
                    TaskType = e.TaskType;
                    break;

                case V1.ProjectTaskEstimationChanged e:
                    TaskEstimation = TaskEstimation.FromDecimal(e.TaskEstimation);
                    break;

                case V1.ProjectTaskWorkStarted e:
                    TaskStatus = ProjectTaskStatus.InProgress;
                    break;

                case V1.ProjectTaskComplited e:
                    TaskStatus |= ProjectTaskStatus.Done;
                    break;
            }
        }
    }
}