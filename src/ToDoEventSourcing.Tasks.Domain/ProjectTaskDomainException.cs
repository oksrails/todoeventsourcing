﻿using ToDoEventSourcing.Lib.Common;

namespace ToDoEventSourcing.ProjectTasks.Domain
{
    public class ProjectTaskDomainException : DomainBaseException
    {
        public ProjectTaskDomainException(string message) : base(message) { }
    }
}
