﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoEventSourcing.Lib.Common;
using static System.String;

namespace ToDoEventSourcing.ProjectTasks.Domain
{
    public class TaskTitle : Value<TaskTitle>
    {
        public string Value { get; }

        internal TaskTitle(string value) => Value = value;

        protected TaskTitle() { }

        public static TaskTitle FromString(string value)
        {
            CheckValidity(value);
            return new TaskTitle(value);
        }

        static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new ProjectTaskDomainException("Task title cannot be empty");

            if (value.Length < 10)
                throw new ProjectTaskDomainException("Task title cannot be shorter then 10 characters");

            if (value.Length > 100)
                throw new ProjectTaskDomainException("Task title cannot be longer then 100 characters");
        }

        public static implicit operator string(TaskTitle taskTitle) => taskTitle.Value;
    }
}
