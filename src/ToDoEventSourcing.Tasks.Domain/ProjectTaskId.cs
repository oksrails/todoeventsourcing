﻿using ToDoEventSourcing.Lib.Common;

namespace ToDoEventSourcing.ProjectTasks.Domain
{
    public class ProjectTaskId : AggregateId<ProjectTask>
    {
        ProjectTaskId(Guid value) : base(value) { }

        public static ProjectTaskId FromGuid(Guid value) => new ProjectTaskId(value);
    }
}
