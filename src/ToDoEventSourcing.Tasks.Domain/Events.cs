﻿namespace ToDoEventSourcing.ProjectTasks.Domain
{
    public static class Events
    {
        public static class V1
        {
            public record ProjectTaskCreated
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
            }

            public record ProjectTaskTitleChanged
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
                public string TaskTitle { get; init; }
            }

            public record ProjectTaskDescriptionChanged
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
                public string TaskDescription { get; init; }
            }

            public record ProjectTaskTypeChanged
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
                public TaskType TaskType { get; init; }
            }

            public record ProjectTaskEstimationChanged
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
                public decimal TaskEstimation { get; init; }
            }

            public record ProjectTaskAssigned
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
                public Guid AssignedUserId { get; init; }
            }

            public record ProjectTaskWorkStarted
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
            }

            public record ProjectTaskComplited
            {
                public Guid Id { get; init; }
                public Guid ProjectId { get; init; }
            }
        }
    }
}
