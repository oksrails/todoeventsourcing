﻿using ToDoEventSourcing.Lib.Common;
using static System.String;

namespace ToDoEventSourcing.Users.Domain
{
    public class DisplayName : Value<DisplayName>
    {
        public string Value { get; }

        internal DisplayName(string value) => Value = value;

        protected DisplayName() { }

        public static DisplayName FromString(string value)
        {
            CheckValidity(value);
            return new DisplayName(value);
        }

        static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new UserDomainException("Display name cannot be empty");
        }

        public static implicit operator string(DisplayName displayName) => displayName.Value;
    }
}
