﻿using ToDoEventSourcing.Lib.Common;
using static ToDoEventSourcing.Users.Domain.Events;

namespace ToDoEventSourcing.Users.Domain
{
    public class UserProfile : AggregateRoot
    {
        FirstName FirstName { get; set; }

        LastName LastName { get; set; }

        DisplayName DisplayName { get; set; }

        public static UserProfile Create(
            UserProfileId id,
            FirstName firstName,
            LastName lastName,
            DisplayName displayName)
        {
            var userProfile = new UserProfile();

            userProfile.Apply(
                new V1.UserCreated
                {
                    UserProfileId = id,
                    FirstName = firstName,
                    LastName = lastName,
                    DisplayName = displayName
                });

            return userProfile;
        }

        public V1.UserFirstNameChanged ChangeFirstName(FirstName firstName)
        {
            var firstNameChanged = new V1.UserFirstNameChanged
            {
                UserProfileId = Id,
                FirstName = firstName
            };

            Apply(firstNameChanged);
            return firstNameChanged;
        }

        public V1.UserLastNameChanged ChangeLastName(LastName lastName)
        {
            var lastNameChanged = new V1.UserLastNameChanged
            {
                UserProfileId = Id,
                LastName = lastName
            };
            Apply(lastNameChanged);
            return lastNameChanged;
        }

        public V1.UserDisplayNameChanged ChangeDisplayName(DisplayName displayName)
        {
            var displayNameChanged = new V1.UserDisplayNameChanged
            {
                UserProfileId = Id,
                DisplayName = displayName
            };
            Apply(displayNameChanged);
            return displayNameChanged;
        }

        protected override void EnsureValidState()
        {
            
        }

        protected override void When(object evt)
        {
            switch (evt)
            {
                case V1.UserCreated e:
                    Id = e.UserProfileId;
                    FirstName = FirstName.FromString(e.FirstName);
                    LastName = LastName.FromString(e.LastName);
                    DisplayName = DisplayName.FromString(e.DisplayName);
                    break;
                case V1.UserFirstNameChanged e:
                    FirstName = FirstName.FromString(e.FirstName);
                    break;

                case V1.UserLastNameChanged e:
                    LastName = LastName.FromString(e.LastName);
                    break;

                case V1.UserDisplayNameChanged e:
                    DisplayName = DisplayName.FromString(e.DisplayName);
                    break;
            }
        }
    }
}