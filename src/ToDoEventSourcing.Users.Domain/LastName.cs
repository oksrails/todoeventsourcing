﻿using ToDoEventSourcing.Lib.Common;
using static System.String;

namespace ToDoEventSourcing.Users.Domain
{
    public class LastName : Value<LastName>
    {
        public string Value { get; }

        internal LastName(string value) => Value = value;

        protected LastName() { }

        public static LastName FromString(string value)
        {
            CheckValidity(value);
            return new LastName(value);
        }

        public static implicit operator string(LastName lastName) => lastName.Value;

        static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new UserDomainException("Last name cannot be empty");
        }
    }
}
