﻿using ToDoEventSourcing.Lib.Common;

namespace ToDoEventSourcing.Users.Domain
{
    public class UserDomainException : DomainBaseException
    {
        public UserDomainException(string message) : base(message) { }
    }
}
