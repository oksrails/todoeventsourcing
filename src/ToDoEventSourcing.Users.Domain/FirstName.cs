﻿using ToDoEventSourcing.Lib.Common;
using static System.String;

namespace ToDoEventSourcing.Users.Domain
{
    public class FirstName : Value<FirstName>
    {
        public string Value { get; }

        internal FirstName(string value) => Value = value;

        protected FirstName() { }

        public static FirstName FromString(string value)
        {
            CheckValidity(value);
            return new FirstName(value);
        }

        public static implicit operator string(FirstName firstName) => firstName.Value;

        static void CheckValidity(string value)
        {
            if (IsNullOrEmpty(value))
                throw new UserDomainException("First name cannot be empty");
        }
    }
}
