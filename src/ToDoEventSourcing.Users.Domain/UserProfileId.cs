﻿using ToDoEventSourcing.Lib.Common;

namespace ToDoEventSourcing.Users.Domain
{
    public class UserProfileId : AggregateId<UserProfile>
    {
        public UserProfileId(Guid value) : base(value) { }

        public static UserProfileId FromGuid(Guid value) => new UserProfileId(value);
    }
}
