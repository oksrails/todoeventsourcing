﻿namespace ToDoEventSourcing.Users.Domain
{
    public static class Events
    {
        public static class V1
        {
            public record UserCreated
            {
                public Guid UserProfileId { get; init; }
                public string FirstName { get; init; }
                public string LastName { get; init; }
                public string DisplayName { get; init; }
            }

            public record UserFirstNameChanged
            {
                public Guid UserProfileId { get; init; }
                public string FirstName { get; init; }
            }

            public record UserLastNameChanged
            {
                public Guid UserProfileId { get; init; }
                public string LastName { get; init; }
            }

            public record UserDisplayNameChanged
            {
                public Guid UserProfileId { get; init; }
                public string DisplayName { get; init; }
            }
        }
    }
}
