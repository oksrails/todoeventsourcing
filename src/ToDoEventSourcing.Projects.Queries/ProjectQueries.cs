﻿using Raven.Client.Documents;
using static ToDoEventSourcing.Projects.Queries.Projections.ReadModels;

namespace ToDoEventSourcing.Projects.Queries
{
    public static class ProjectQueries
    {
        public static async Task<ProjectDetails> GetProjectDetails(
            this GetProjectsModuleSession getSession,
            Guid id)
        {
            using var session = getSession();

            return await session.LoadAsync<ProjectDetails>(ProjectDetails.GetDatabaseId(id));
        }

        public static async Task<List<ProjectDetails>> GetProjects(
            this GetProjectsModuleSession getSession)
        {
            using var session = getSession();
            return await session.Query<ProjectDetails>().ToListAsync();
        }
    }
}