﻿using Microsoft.AspNetCore.Mvc;
using Raven.Client.Documents.Session;
using ToDoEventSourcing.Lib.RavenDb;
using ToDoEventSourcing.Projects.Queries.Projections;
using static ToDoEventSourcing.Projects.Queries.Projections.ReadModels;

namespace ToDoEventSourcing.Projects.Queries
{
    [Route("/project")]
    public class ProjectQueryApi : ControllerBase
    {
        readonly GetProjectsModuleSession _getSession;

        public ProjectQueryApi(GetProjectsModuleSession getSession)
            => _getSession = getSession;

        [HttpGet("projectDetails")]
        public async Task<ActionResult<ProjectDetails>> Get(
            [FromQuery] QueryModels.GetProjectDetailsQuery request)
        {
            var project = await _getSession.GetProjectDetails(request.ProjectId);

            if (project == null)
                return NotFound();

            return Ok(project);
        }

        [HttpGet]
        public async Task<ActionResult<List<ProjectDetails>>> GetProjects()
        {
            var projects = await _getSession.GetProjects();
            if (projects == null)
                return NotFound();

            return Ok(projects);
        }
    }
}
