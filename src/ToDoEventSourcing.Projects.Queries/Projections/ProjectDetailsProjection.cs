﻿using Raven.Client.Documents.Session;
using Raven.Client.Documents;
using ToDoEventSourcing.Lib.RavenDb;
using static ToDoEventSourcing.Projects.Domain.Events;

namespace ToDoEventSourcing.Projects.Queries.Projections
{
    public static class ProjectDetailsProjection
    {
        public static Func<Task> GetHanlder(
            IAsyncDocumentSession session,
            object @event)
        {
            Func<Guid, string> getDbId = ReadModels.ProjectDetails.GetDatabaseId;

#pragma warning disable CS8603 // Possible null reference return.
            return @event switch
            {
                V1.ProjectCreated e =>
                    () => Create(e.Id, e.ProjectOwnerId),
                V1.ProjectTitleChanged e =>
                    () => Update(e.Id, p => p.ProjectTitle = e.ProjectTitle),
                V1.ProjectDescriptionChanged e =>
                    () => Update(e.Id, p => p.ProjectDescription = e.ProjectDescription),
                _ => (Func<Task>)null
            };
#pragma warning restore CS8603 // Possible null reference return.

            string GetDbId(Guid id) => ReadModels.ProjectDetails.GetDatabaseId(id);

            async Task<string> GetProjectOwnerName(Guid projectOwnerId)
            {
                var store = session.Advanced.DocumentStore;
                var userSession = store.OpenAsyncSession("Users");

                var projectOwner = await userSession.LoadAsync<Users.Queries.Projections.ReadModels.UserDetails>($"UserDetails/{projectOwnerId}");
                return projectOwner.DisplayName;
            }

            Task Create(Guid id, Guid ownerId)
                => session.Create<ReadModels.ProjectDetails>(
                    async p =>
                    {
                        p.Id = GetDbId(id);
                        p.ProjectOwnerId = ownerId;
                        p.ProjectOwnerName = GetProjectOwnerName(ownerId).Result;
                    });

            Task Update(Guid id, Action<ReadModels.ProjectDetails> update)
                => session.Update(getDbId(id), update);
        }
    }
}
