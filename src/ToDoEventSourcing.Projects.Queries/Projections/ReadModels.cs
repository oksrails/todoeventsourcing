﻿namespace ToDoEventSourcing.Projects.Queries.Projections
{
    public class ReadModels
    {
        public class ProjectDetails
        {
            public string Id { get; set; }
            public Guid ProjectOwnerId { get; set; }
            public string ProjectOwnerName { get; set; }
            public string ProjectTitle { get; set; }
            public string ProjectDescription { get; set; }

            public static string GetDatabaseId(Guid id) => $"ProjectDetails/{id}";
        }
    }
}
