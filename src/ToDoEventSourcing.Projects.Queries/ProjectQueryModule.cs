﻿using EventStore.ClientAPI;
using Microsoft.Extensions.DependencyInjection;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using ToDoEventSourcing.Lib.EventSourcing;
using ToDoEventSourcing.Lib.EventStore;
using ToDoEventSourcing.Lib.RavenDb;
using ToDoEventSourcing.Projects.Queries.Projections;
namespace ToDoEventSourcing.Projects.Queries
{
    public static class ProjectQueryModule
    {
        const string SubscriptionName = "projectSubscription";

        public static IServiceCollection AddProjectsSelectionModule(
            this IServiceCollection services,
            string databaseName)
        {
            EventMappings.MapEventTypes();

            services
                .AddSingleton<GetProjectsModuleSession>(
                    c =>
                    {
                        var store = c.GetRequiredService<IDocumentStore>();
                        store.CheckAndCreateDatabase(databaseName);

                        IAsyncDocumentSession GetSession()
                            => store.OpenAsyncSession(databaseName);

                        return GetSession;
                    }
                );

            return services;
        }

        public static IServiceCollection AddProjectProjectionModule(
            this IServiceCollection services,
            string databaseName)
        {
            EventMappings.MapEventTypes();

            services.AddSingleton(s =>
            {
                var store = s.GetRavenStore();
                store.CheckAndCreateDatabase(databaseName);

                IAsyncDocumentSession GetSession()
                    => s.GetRavenStore().OpenAsyncSession(databaseName);

                return new SubscriptionManager(
                    new RavenDbCheckpointStore(GetSession, SubscriptionName),
                    s.GetEsConnection(),
                    SubscriptionName,
                    StreamName.AllStream,
                    new RavenDbProjection<ReadModels.ProjectDetails>(GetSession, ProjectDetailsProjection.GetHanlder));
            });

            return services;
        }

        static IDocumentStore GetRavenStore(
            this IServiceProvider provider
        )
            => provider.GetRequiredService<IDocumentStore>();

        static IEventStoreConnection GetEsConnection(this IServiceProvider provider)
            => provider.GetRequiredService<IEventStoreConnection>();

        static IAggregateStore GetAggregateStore(this IServiceProvider provider)
            => provider.GetRequiredService<IAggregateStore>();
    }

    public delegate IAsyncDocumentSession GetProjectsModuleSession();
}
