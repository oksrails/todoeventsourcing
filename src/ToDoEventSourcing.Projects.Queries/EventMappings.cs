﻿using static ToDoEventSourcing.Lib.EventSourcing.TypeMapper;
using static ToDoEventSourcing.Projects.Domain.Events;

namespace ToDoEventSourcing.Projects.Queries
{
    internal static class EventMappings
    {
        public static void MapEventTypes()
        {
            Map<V1.ProjectCreated>("ProjectCreated");
            Map<V1.ProjectTitleChanged>("ProjectTitleChanged");
            Map<V1.ProjectDescriptionChanged>("ProjectDescriptionChanged");
        }
    }
}
