﻿namespace ToDoEventSourcing.Projects.Queries
{
    public class QueryModels
    {
        public record GetProjectDetailsQuery
        {
            public Guid ProjectId { get; set; }
        }

        public record GetProjects()
        {
            public int Page { get; set; }
            public int PageSize { get; set; }
        }
    }
}
